package main

import "fmt"

func main() {
	n := 12
	// Read n from input
	DisplayMinimumNumberFunction(n)
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) {
	var min_func string = "min(int, int)"

	for i := 2; i < n; i++ {
		min_func = "min(int, " + min_func + ")"
	}

	fmt.Printf("%s", min_func)
}
