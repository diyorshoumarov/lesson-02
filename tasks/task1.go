package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		FizzBuzz(i)
	}
}

func FizzBuzz(n int) {
	if n%15 == 0 {
		fmt.Println("FizzBuzz")
	} else if n%5 == 0 {
		fmt.Println("Fizz")
	} else if n%3 == 0 {
		fmt.Println("Buzz")
	} else {
		fmt.Println(n)
	}
}
